import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from "@ngrx/effects";
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HttpInterceptorModule } from 'ng-http-interceptor';
import { RoutingModule } from './routing.module';
import { SharedModule } from './shared/shared.module';

import { AuthGuardLogin } from './services/auth-guard-login.service';
import { AuthGuardAdmin } from './services/auth-guard-admin.service';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { TranslateService } from './services/translate.service';
import { CityService } from './services/city.service';
import { CountryService } from './services/country.service';
import { CurrencyService } from './services/currency.service';
import { GeoLocationService } from './services/geoLocation.service';
import { LanguageService } from './services/language.service';
import { MetainfoService } from './services/metainfo.service';
import { StateService } from './services/state.service';
import { ConfirmationService } from 'primeng/primeng'

import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SystemComponent } from './system/system.component';

import { CityEffects } from './redux/effects/city.effect';
import { CountryEffects } from './redux/effects/country.effect';
import { CurrencyEffects } from './redux/effects/currency.effect';
import { GeoLocationEffects } from './redux/effects/geoLocation.effect';
import { LanguageEffects } from './redux/effects/language.effect';
import { StateEffects } from './redux/effects/state.effect';

import { cityReducer } from './redux/reducers/city.reducer';
import { countryReducer } from './redux/reducers/country.reducer';
import { currencyReducer } from './redux/reducers/currency.reducer';
import { geoLocationReducer } from './redux/reducers/geoLocation.reducer';
import { languageReducer } from './redux/reducers/language.reducer';
import { stateReducer } from './redux/reducers/state.reducer';

import { initialState } from './redux/app.state';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    NotFoundComponent,
    DashboardComponent,
    SystemComponent
  ],
  imports: [
    RoutingModule,
    SharedModule,
    StoreModule.forRoot(<any>{
      cities: cityReducer,
      countries: countryReducer,
      currencies: currencyReducer,
      geoLocations: geoLocationReducer,
      languages: languageReducer,
      states: stateReducer
    }, { initialState }),
    EffectsModule.forRoot([
      CityEffects,
      CountryEffects,
      CurrencyEffects,
      GeoLocationEffects,
      LanguageEffects,
      StateEffects
    ]),
    StoreDevtoolsModule.instrument({ maxAge: 10 }),
    HttpInterceptorModule.noOverrideHttp()
  ],
  providers: [
    ConfirmationService,
    AuthGuardLogin,
    AuthGuardAdmin,
    AuthService,
    UserService,
    TranslateService,
    CityService,
    CountryService,
    CurrencyService,
    GeoLocationService,
    LanguageService,
    MetainfoService,
    StateService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule { }
