import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SystemComponent } from './system/system.component';

import { AuthGuardLogin } from './services/auth-guard-login.service';
import { AuthGuardAdmin } from './services/auth-guard-admin.service';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuardLogin] },
  { path: 'login', component: LoginComponent },
  { path: 'cities', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'countries', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'currencies', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'geoLocations', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'languages', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'states', component: SystemComponent, canActivate: [AuthGuardLogin] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuardLogin] },
  { path: 'notfound', component: NotFoundComponent },
  { path: '**', redirectTo: '/notfound' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RoutingModule { }
