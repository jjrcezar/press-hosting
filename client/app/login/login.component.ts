import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DynamicFormComponent } from '../shared/dynamic-form/dynamic-form.component';

import { Message } from 'primeng/primeng';

import { AuthService } from '../services/auth.service';
import { TranslateService } from '../services/translate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  isLoading: boolean = false;

  loginForm: FormGroup;
  username = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]);
  password = new FormControl('', [Validators.required, Validators.minLength(6)]);

  msgs: Message[] = [];

  constructor(private _auth: AuthService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _translate: TranslateService) {
  }

  ngOnInit() {
    if (this._auth.loggedIn) {
      this._router.navigate(['/']);
    }
    this.loginForm = this._formBuilder.group({
      username: this.username,
      password: this.password
    });
  }

  ngAfterViewInit() { }

  setClassUsername() {
    return { 'has-danger': !this.username.pristine && !this.username.valid };
  }
  setClassPassword() {
    return { 'has-danger': !this.password.pristine && !this.password.valid };
  }

  login() {
    this.msgs = [];
    this.isLoading = true;
    this._auth.adminLogin(this.loginForm.value).subscribe(
      res => {
        this.isLoading = false;
        if (res.success === false) {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: '', detail: this._translate.instant(res.message) });
        } else {
          this._router.navigate(['/']);
        }
      },
      error => {
        this.isLoading = false;
        this.msgs.push({ severity: 'error', summary: '', detail: this._translate.instant('login.failed.message') });
      }
    );
  }

}
