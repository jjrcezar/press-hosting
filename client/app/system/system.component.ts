import { Component, ViewChild, ElementRef, ViewChildren, QueryList, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";

import { ConfirmationService } from 'primeng/primeng';
import { TranslateService } from '../services/translate.service';

import { AppState } from '../redux/app.state';
import { EntityMemberMeta } from '../redux/app.state';

import * as CityActions from '../redux/actions/city.action';
import * as CountryActions from '../redux/actions/country.action';
import * as CurrencyActions from '../redux/actions/currency.action';
import * as GeoLocationActions from '../redux/actions/geoLocation.action';
import * as LanguageActions from '../redux/actions/language.action';
import * as StateActions from '../redux/actions/state.action';

import { ElementConfig } from '../shared/dynamic-form/models/element-config.interface';

const entityNameMap: { [type: string]: string } = {
    city: 'cities',
    country: 'countries',
    currency: 'currencies',
    geoLocation: 'geoLocations',
    language: 'languages',
    state: 'states'
};

const reduxActionsMap: { [type: string]: any } = {
    cities: CityActions,
    countries: CountryActions,
    currencies: CurrencyActions,
    geoLocations: GeoLocationActions,
    languages: LanguageActions,
    states: StateActions
};

const memberMetaToConfigMap: { [type: string]: string } = {
    name: "name",
    dataType: "type",
    readOnly: "disabled",
    label: "label",
    description: "description",
    hint: "hint",
};

const validationMetaToConfigMap: { [type: string]: string } = {
    type: "validation_type",
    required: "validation_required",
    format: "validation_format",
    maxLength: "validation_maxLength",
    values: "validation_values"
}

@Component({
    selector: 'app-system',
    templateUrl: './system.component.html',
    styleUrls: ['./system.component.scss']
})
export class SystemComponent implements OnInit, AfterViewInit, OnDestroy {

    // @ViewChild('editEntryForm') editEntryForm: ElementRef;
    @ViewChildren("editEntryForm")
    form: QueryList<any>;
    editEntryForm: any;
    
    entityId: any;
    entityName: any;
    entityActions: any;

    entityTableFields: any[];
    entityTableFieldLabels: { [key: string]: string };
    entityFormInputConfigs: ElementConfig[];
    entityFormValues$: Observable<any[]>;

    entityTableEntries$: Observable<any[]>;
    entityDependencies$: { [key: string]: Observable<any[]> } = {};

    loading$: Observable<boolean>;

    subscription: any;


    constructor(
        private _store$: Store<AppState>,
        private _router: Router,
        private _confirmationService: ConfirmationService,
        private _translateService: TranslateService) {
        this.entityName = this._router.url.substr(1);
        this.entityActions = reduxActionsMap[this.entityName];
    }

    ngOnInit() {
        this._store$.dispatch(new this.entityActions.GetMetadata());
        this._store$.dispatch(new this.entityActions.GetEntries());

        this.loading$ = this._store$.select(this.entityName, 'loading');

        this._handleSubscription(this._store$.select(this.entityName, 'metadata').subscribe(members => {
            this.entityTableFields = [];
            this.entityTableFieldLabels = {};
            this.entityFormInputConfigs = [];

            this.entityTableEntries$ = this._store$.select(this.entityName).map(l => l.ids.map(i => l.entries[i]));
            for (var key in members) {
                if (key.toLowerCase().includes('id')) {
                    this._dispatchGetDependency(key);
                    this._subscribeToDependency(key);
                }
                this.entityTableFields.push(key);
                this.entityTableFieldLabels[key] = members[key].label;
                this.entityFormInputConfigs.push(this._mapMetadataToElementConfig(key, members[key]));
            }
        }));
    }

    ngAfterViewInit() {
        this.form.changes.subscribe((forms: QueryList<any>) => {
            this.editEntryForm = forms.first;
        });

    }

    private _mapMetadataToElementConfig(name, meta): ElementConfig {
        let config: any = { name: name };
        for (let prop in meta) {
            let configProp = memberMetaToConfigMap[prop];
            if (configProp) config[configProp] = meta[prop];
        }
        for (let prop in meta["validation"]) {
            let configProp = validationMetaToConfigMap[prop];
            if (configProp) config[configProp] = meta["validation"][prop];
        }
        return config;
    }

    private _subscribeToDependency(key) {
        let entityName: any = entityNameMap[key.slice(0, -2)];
        if (!(key in Object.keys(this.entityDependencies$))) {
            this.entityDependencies$[key] = this._store$.select(entityName).map(l => l.ids.map(i => {
                return { value: i, label: (l.entries[i].name || ('Latitude: ' + l.entries[i].latitude + '; Longitude: ' + l.entries[i].longitude)) };
            }));
        }
    }

    private _dispatchGetDependency(key) {
        let entityName: any = entityNameMap[key.slice(0, -2)];
        let action: any = reduxActionsMap[entityName];
        this._store$.dispatch(new action.GetEntries());
    }

    private _handleSubscription(subscription) {
        if (this.subscription) {
            this.subscription.add(subscription);
        } else {
            this.subscription = subscription;
        }
    }

    private _getSingular(entity) {
        let singular = '';
        for (let key in entityNameMap) {
            if (entityNameMap[key] === entity) {
                singular = key;
                break;
            }
        }
        return singular;
    }

    private _getEntry(data) {
        this.entityId = data.id;
        for (let field in data) {
            this.editEntryForm.setValue(field, data[field]);
        }
    }

    private _saveEntry(formValues) {
        this._store$.dispatch(new this.entityActions.SaveEntry(formValues));
    }

    private _editEntry(formValues) {
        formValues = {...formValues, id: this.entityId};
        this._store$.dispatch(new this.entityActions.UpdateEntry({entry: formValues, id: this.entityId}));
    }

    private _confirmDeleteEntry(id) {
        this._confirmationService.confirm({
            header: this._translateService.instant('confirm.delete.title'),
            message: this._translateService.instant('confirm.delete.msg'),
            accept: () => {
                this._store$.dispatch(new this.entityActions.DeleteEntry(id));
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
