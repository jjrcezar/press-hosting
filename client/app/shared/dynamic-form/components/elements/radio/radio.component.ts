import { Component, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { Element } from '../../../models/element.interface';
import { ElementConfig } from '../../../models/element-config.interface';

import { TranslateService } from '../../../../../services/translate.service';

import { getValidationErrorMessages } from '../../../dynamic-form.component';

@Component({
  selector: 'form-radio',
  templateUrl: 'radio.component.html',
  styleUrls: ['radio.component.scss']
})
export class RadioComponent implements Element {
  config: ElementConfig;
  group: FormGroup;
  dependency$:  Observable<any[]>;
  
  constructor(private _translate: TranslateService) { }

  get name() { return this.config.name; }

  get errorMessages() {
    return getValidationErrorMessages(this.group.controls[this.name].errors, this.config.validation_type, this.config.validation_maxLength);
  }
}
