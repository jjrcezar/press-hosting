import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { Element } from '../../../models/element.interface';
import { ElementConfig } from '../../../models/element-config.interface';
import { SelectItem } from 'primeng/primeng';
import { TranslateService } from '../../../../../services/translate.service';

import { getValidationErrorMessages } from '../../../dynamic-form.component';

@Component({
  selector: 'form-dropdown',
  templateUrl: 'dropdown.component.html',
  styleUrls: ['dropdown.component.scss']
})
export class DropdownComponent implements Element, OnInit {
  config: ElementConfig;
  group: FormGroup;
  dependency$: Observable<any[]>;
  dropdownOptions: any[] = [];

  subscription: any;

  constructor(private _translate: TranslateService) {

  }

  get name() { return this.config.name; }

  get errorMessages() {
    return getValidationErrorMessages(this.group.controls[this.name].errors, this.config.validation_type, this.config.validation_maxLength);
  }

  ngOnInit() {
    this.subscription = this.dependency$.subscribe(items => {
      this.dropdownOptions = [{ label: "Please select one", value: null }, ...items];
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
