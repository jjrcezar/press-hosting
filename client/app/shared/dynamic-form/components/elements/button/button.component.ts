import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { Element } from '../../../models/element.interface';
import { ElementConfig } from '../../../models/element-config.interface';

@Component({
  selector: 'form-button',
  templateUrl: 'button.component.html',
  styleUrls: ['button.component.scss']
})
export class ButtonComponent implements Element {
  config: ElementConfig;
  group: FormGroup;
  dependency$:  Observable<any[]>;
}
