import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { Element } from '../../../models/element.interface';
import { ElementConfig } from '../../../models/element-config.interface';

@Component({
  selector: 'form-select',
  templateUrl: 'select.component.html',
  styleUrls: ['select.component.scss']
})
export class SelectComponent implements Element {
  config: ElementConfig;
  group: FormGroup;
  dependency$:  Observable<any[]>;
}
