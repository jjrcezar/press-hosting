import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnChanges, OnInit, Type, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { ButtonComponent } from '../elements/button/button.component';
import { InputComponent } from '../elements/input/input.component';
import { SelectComponent } from '../elements/select/select.component';
import { RadioComponent } from '../elements/radio/radio.component';
import { DropdownComponent } from '../elements/dropdown/dropdown.component';

import { Element } from '../../models/element.interface';
import { ElementConfig } from '../../models/element-config.interface';

const components: { [type: string]: Type<Element> } = {
    button: ButtonComponent,
    string: InputComponent,
    long: InputComponent,
    double: InputComponent,
    select: SelectComponent,
    enum: RadioComponent,
    dropdown: DropdownComponent
};

@Directive({
    selector: '[dynamicField]'
})
export class DynamicFieldDirective implements Element, OnChanges, OnInit {
    @Input()
    config: ElementConfig;

    @Input()
    dependency$: Observable<any[]>;

    @Input()
    group: FormGroup;

    component: ComponentRef<Element>;

    constructor(
        private resolver: ComponentFactoryResolver,
        private container: ViewContainerRef
    ) { }

    ngOnChanges() {
        if (this.component) {
            this.component.instance.config = this.config;
            this.component.instance.group = this.group;
        }
    }

    ngOnInit() {
        if (!components[this.config.type]) {
            const supportedTypes = Object.keys(components).join(', ');
            throw new Error(
                `Trying to use an unsupported type (${this.config.type}).
        Supported types: ${supportedTypes}`
            );
        }
        if (this.config.validation_type === 'enum') this.config.type = 'enum';
        if (this.config.type === 'long') this.config.type = 'dropdown';

        const component = this.resolver.resolveComponentFactory<Element>(components[this.config.type]);
        this.component = this.container.createComponent(component);
        this.component.instance.config = this.config;
        this.component.instance.group = this.group;
        if (this.config.type === 'dropdown') this.component.instance.dependency$ = this.dependency$;
    }
}
