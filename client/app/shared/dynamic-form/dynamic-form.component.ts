import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { ElementConfig } from './models/element-config.interface';

const validationTypeMap: { [type: string]: string } = {
    alphanumeric: "^[a-zA-Z0-9\\s]*$",
    numeric: "^[0-9.]*$"
}

const validationErrorMessageMap: { [type: string]: string } = {
    required: "error.message.required",
    alphanumeric: "error.message.alphanumeric",
    numeric: "error.message.numeric",
    maxlength: "error.message.maxlength"
}

export const getValidationErrorMessages = (errors, pattern, maxlength) => {
    let err: { message: string, expected: any }[] = [];
    for (let error in errors) {
        switch (error) {
            case 'required':
                err = [...err, { message: validationErrorMessageMap[error], expected: null }];
                break;
            case 'pattern':
                err = [...err, { message: validationErrorMessageMap[pattern], expected: null }];
                break;
            case 'maxlength':
                err = [...err, { message: validationErrorMessageMap[error], expected: maxlength }];
                break;
        }
    }
    return err;
}

@Component({
    exportAs: 'dynamicForm',
    selector: 'dynamic-form',
    templateUrl: 'dynamic-form.component.html',
    styleUrls: ['dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnChanges, OnInit {
    @Input()
    config: ElementConfig[] = [];

    @Input()
    operation: string;

    @Input()
    dependencies$: { [key: string]: Observable<any[]> };

    @Output()
    submit: EventEmitter<any> = new EventEmitter<any>();

    form: FormGroup;

    get controls() { return this.config.filter(({ type }) => type !== 'button'); }
    get changes() { return this.form.valueChanges; }
    get valid() { return this.form.valid; }
    get value() { return this.form.value; }

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.form = this.createGroup();
    }

    ngOnChanges() {
        if (this.form) {
            const controls = Object.keys(this.form.controls);
            const configControls = this.controls.map((item) => item.name);

            controls
                .filter((control) => !configControls.includes(control))
                .forEach((control) => this.form.removeControl(control));

            configControls
                .filter((control) => !controls.includes(control))
                .forEach((name) => {
                    const config = this.config.find((control) => control.name === name);
                    this.form.addControl(name, this.createControl(config));
                });

        }
    }

    ngOnDestroy() { }

    createGroup() {
        const group = this.fb.group({});
        this.controls.forEach(control => group.addControl(control.name, this.createControl(control)));
        return group;
    }

    createControl(config: ElementConfig) {
        const { disabled, value } = config;
        return this.fb.control({ disabled, value }, this.createValidations(config));
    }

    createValidations(config: ElementConfig) {
        const { validation_type, validation_required, validation_maxLength } = config;
        let validations: ValidatorFn[] = [];
        if (validationTypeMap[validation_type]) validations = [...validations, Validators.pattern(validationTypeMap[validation_type])];
        if (validation_maxLength) validations = [...validations, Validators.maxLength(+validation_maxLength)];
        if (validation_required) validations = [...validations, Validators.required];
        return validations;
    }

    handleSubmit(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        this.submit.emit(this.value);
    }

    setDisabled(name: string, disable: boolean) {
        if (this.form.controls[name]) {
            const method = disable ? 'disable' : 'enable';
            this.form.controls[name][method]();
            return;
        }

        this.config = this.config.map((item) => {
            if (item.name === name) {
                item.disabled = disable;
            }
            return item;
        });
    }

    setValue(name: string, value: any) {
        this.form.controls[name] && this.form.controls[name].setValue(value, { emitEvent: true });
    }
}
