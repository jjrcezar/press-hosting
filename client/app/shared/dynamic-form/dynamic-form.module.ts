import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PrimengModules } from '../primeng.module';

import { DynamicFieldDirective } from './components/directive/dynamic-field.directive';
import { ButtonComponent } from './components/elements/button/button.component';
import { InputComponent } from './components/elements/input/input.component';
import { SelectComponent } from './components/elements/select/select.component';
import { RadioComponent } from './components/elements/radio/radio.component';
import { DropdownComponent } from './components/elements/dropdown/dropdown.component';
import { DynamicFormComponent } from './dynamic-form.component';
import { TranslatePipe } from '../i18n/translate.pipe';
import { TranslateProvider } from '../i18n/translate.provider';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        PrimengModules
    ],
    declarations: [
        DynamicFieldDirective,
        DynamicFormComponent,
        ButtonComponent,
        InputComponent,
        SelectComponent,
        RadioComponent,
        DropdownComponent,
        TranslatePipe
    ],
    exports: [
        DynamicFormComponent,
        TranslatePipe
    ],
    entryComponents: [
        ButtonComponent,
        InputComponent,
        SelectComponent,
        RadioComponent,
        DropdownComponent
    ],
    providers: [
        TranslateProvider
    ]
})
export class DynamicFormModule { }
