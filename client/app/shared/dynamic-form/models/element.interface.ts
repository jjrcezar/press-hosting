import { FormGroup } from '@angular/forms';
import { ElementConfig } from './element-config.interface';
import { Observable } from "rxjs/Observable";

export interface Element {
    config: ElementConfig,
    group: FormGroup
    dependency$: Observable<any[]>;
}
