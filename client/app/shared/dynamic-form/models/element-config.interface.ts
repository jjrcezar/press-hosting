export interface ElementConfig {
    name: string,
    type: string,
    disabled: boolean,
    label?: string,
    description?: string,
    hint?: string,
    validation_type: string,
    validation_required: boolean,
    validation_format?: string,
    validation_maxLength?: number,
    validation_values?: string[],
    validation_value_pair?: {value: number, label: string}[],
    value: any;
}
