import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimengModules } from './primeng.module';
import { HttpModule } from '@angular/http';

import { AppTopBar } from './topbar/topbar.component';
import { AppFooter } from './footer/footer.component';
import { AppMenuComponent, AppSubMenu } from './menu/menu.component';
import { InlineProfileComponent } from './profile/profile.component';

import { DynamicFormModule } from './dynamic-form/dynamic-form.module';

import { LoadingComponent } from './loading/loading.component';

//common pipes
import { TitleCase } from './pipes/titlecase.pipe';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    PrimengModules,
    HttpModule,
    DynamicFormModule,
    HttpModule
  ],
  exports: [
    // Shared Modules
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    PrimengModules,
    DynamicFormModule,
    HttpModule,
    // Shared Components
    LoadingComponent,
    AppTopBar,
    AppFooter,
    AppMenuComponent,
    AppSubMenu,
    InlineProfileComponent,
    TitleCase
  ],
  declarations: [
    LoadingComponent,
    AppTopBar,
    AppFooter,
    AppMenuComponent,
    AppSubMenu,
    InlineProfileComponent,
    TitleCase
  ]
})
export class SharedModule { }
