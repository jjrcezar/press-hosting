import { Component, Inject, forwardRef } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
    selector: 'app-footer',
    templateUrl: 'footer.component.html'
})
export class AppFooter { }