export class Constants {
    public static readonly ADMIN_LOGIN_URL: string = '/adminLogin';
    public static readonly METAINFO_USER_URL: string = '/metainfo/User';
    public static readonly METAINFO_CITY_URL: string = '/metainfo/City';
    public static readonly METAINFO_COUNTRY_URL: string = '/metainfo/Country';
    public static readonly METAINFO_CURRENCY_URL: string = '/metainfo/Currency';
    public static readonly METAINFO_GEOLOCATION_URL: string = '/metainfo/GeoLocation';
    public static readonly METAINFO_STATE_URL: string = '/metainfo/State';
    public static readonly METAINFO_LANGUAGE_URL: string = '/metainfo/Language';
    public static readonly CITIES_URL: string = '/cities';
    public static readonly COUNTRIES_URL: string = '/countries';
    public static readonly CURRENCIES_URL: string = '/currencies';
    public static readonly GEOLOCATIONS_URL: string = '/geoLocations';
    public static readonly LANGUAGES_URL: string = '/languages';
    public static readonly STATES_URL: string = '/states';
}