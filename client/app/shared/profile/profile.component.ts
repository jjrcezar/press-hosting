import { Component, Input, OnInit, EventEmitter, ViewChild, trigger, state, transition, style, animate, Inject, forwardRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { AppComponent } from '../../app.component';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'inline-profile',
    templateUrl: 'profile.component.html',
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class InlineProfileComponent {

    active: boolean;

    constructor(private _auth: AuthService) { }

    onClick(event) {
        this.active = !this.active;
        event.preventDefault();
    }

    logout() {
        this._auth.logout();
    }
}