import { OpaqueToken } from '@angular/core';
import { LOCALE_EN_US, LOCALE_EN_US_TRANSLATIONS } from './lang/en-US';
import { LOCALE_DE_DE, LOCALE_DE_DE_TRANSLATIONS } from './lang/de-DE';
import { LOCALE_JA_JP, LOCALE_JA_JP_TRANSLATIONS } from './lang/ja-JP';

export const TRANSLATIONS = new OpaqueToken('translations');
export const dictionary = {
    [LOCALE_EN_US]: LOCALE_EN_US_TRANSLATIONS,
    [LOCALE_DE_DE]: LOCALE_DE_DE_TRANSLATIONS,
    [LOCALE_JA_JP]: LOCALE_JA_JP_TRANSLATIONS,
};
export const TranslateProvider = [
    { provide: TRANSLATIONS, useValue: dictionary }
];