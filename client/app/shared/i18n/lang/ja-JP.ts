export const LOCALE_JA_JP = 'ja-JP';

export const LOCALE_JA_JP_TRANSLATIONS = {
    'login.welcome' : 'Welcome! Please login.',
    'invalid.user.exception' : '無効なユーザー資格情報',
    'login.failed.message' : 'ログインに失敗しました',

    'entity.cities.s' : 'City',
    'entity.cities.p' : 'Cities',
    'entity.countries.s' : 'Country',
    'entity.countries.p' : 'Countries',
    'entity.currencies.s' : 'Currency',
    'entity.currencies.p' : 'Currencies',
    'entity.geoLocations.s' : 'Geo Location',
    'entity.geoLocations.p' : 'Geo Locations',
    'entity.languages.s' : 'Language',
    'entity.languages.p' : 'Languages',
    'entity.states.s' : 'State',
    'entity.states.p' : 'States',

    'error.message.required': 'This field is required.',
    'error.message.alphanumeric': 'Please enter alphanumeric values only.',
    'error.message.numeric': 'Please enter numeric values only.',
    'error.message.maxlength': 'Maximum number of characters: ',
    
    'hello world': 'hello world JAP',
    'user.dto.userName.label': 'ユーザー名',
    'user.dto.password.label': 'パスワード',
    
    'city.dto.name.label': 'Name',
    'city.dto.status.label': 'Status',
    'city.dto.state.id.label': 'State',
    'city.dto.geolocation.id.label': 'Geo Location',
    'city.dto.language.id.label': 'Language',

    'currency.dto.name.label': 'Name',
    'currency.dto.symbol.label': 'Symbol',
    'currency.dto.isoAlpha3.label': 'ISO Alpha 3',
    'currency.dto.isoNumeric.label': 'ISO Numeric',
    'currency.dto.status.label': 'Status',
    'currency.dto.language.id.label': 'Language',

    'country.dto.name.label': 'Name',
    'country.dto.name.description': 'Name of country',
    'country.dto.name.hint': 'Please input alphanumeric characters only',
    'country.dto.isoAlpha2.label': 'ISO Alpha 2',
    'country.dto.isoAlpha3.label': 'ISO Alpha 3',
    'country.dto.isoNumeric.label': 'ISO Numeric',
    'country.dto.language.id.label': 'Language',
    'country.dto.currency.id.label': 'Currency',
    'country.dto.geoLocation.id.label': 'Geo Location',
    'country.dto.status.label': 'Status',

    'geoLocation.dto.latitude.label': 'Latitude',
    'geoLocation.dto.longitude.label': 'Longitude',
    'geoLocation.dto.status.label': 'Status',

    'language.dto.isoAlpha2.label': 'ISO Alpha 2',
    'language.dto.isoAlpha3.label': 'ISO Alpha 3',
    'language.dto.language.id.label': 'ID',
    'language.dto.name.label': 'Name',
    'language.dto.status.label': 'Status',

    'state.dto.country.id.label': 'Country',
    'state.dto.geoLocation.id.label': 'Geo Location',
    'state.dto.language.id.label': 'Language',
    'state.dto.name.label': 'Name',
    'state.dto.status.label': 'Status'
};