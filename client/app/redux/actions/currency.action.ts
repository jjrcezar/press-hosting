import { Action } from '@ngrx/store';
import { _Currency } from '../models/currency.model';

export const GET_CURRENCY = 'Currency get';
export const GET_CURRENCY_META = 'Currency meta get';
export const GET_CURRENCY_META_SUCCESS = 'Currency meta get success';
export const GET_CURRENCIES = 'Currencies get';
export const GET_CURRENCIES_SUCCESS = 'Currencies get success';
export const SAVE_CURRENCY = "Save new currency";
export const DELETE_CURRENCY = 'Delete currency';
export const UPDATE_CURRENCY = 'Update currency';

export class GetMetadata implements Action {
    readonly type = GET_CURRENCY_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_CURRENCY_META_SUCCESS;
    constructor(public payload: _Currency) { }
}

export class GetEntries implements Action {
    readonly type = GET_CURRENCIES;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_CURRENCIES_SUCCESS;
    constructor(public payload: { entries: { [id: number]: _Currency }, ids: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_CURRENCY;
    constructor(public payload: _Currency) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_CURRENCY;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_CURRENCY;
    constructor(public payload: { entry: _Currency, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;