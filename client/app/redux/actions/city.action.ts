import { Action } from '@ngrx/store';
import { _City } from '../models/city.model';

export const GET_CITY = 'City get';
export const GET_CITY_META = 'City meta get';
export const GET_CITY_META_SUCCESS = 'City meta get success';
export const GET_CITIES = 'Cities get';
export const GET_CITIES_SUCCESS = 'Cities get success';
export const SAVE_CITY = "Save new city";
export const DELETE_CITY = 'Delete city';
export const UPDATE_CITY = 'Update city';

export class GetMetadata implements Action {
    readonly type = GET_CITY_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_CITY_META_SUCCESS;
    constructor(public payload: _City) { }
}

export class GetEntries implements Action {
    readonly type = GET_CITIES;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_CITIES_SUCCESS;
    constructor(public payload: { entries: { [id: number]: _City }, ids: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_CITY;
    constructor(public payload: _City) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_CITY;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_CITY;
    constructor(public payload: { entry: _City, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;