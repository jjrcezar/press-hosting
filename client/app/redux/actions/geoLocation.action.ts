import { Action } from '@ngrx/store';
import { _GeoLocation, _GeoLocationMeta } from '../models/geoLocation.model';

export const GET_GEOLOCATION = 'GeoLocation get';
export const GET_GEOLOCATION_META = 'GeoLocation metadata get';
export const GET_GEOLOCATION_META_SUCCESS = 'GeoLocation metadata get success';
export const GET_GEOLOCATIONS = 'GeoLocations get';
export const GET_GEOLOCATIONS_SUCCESS = 'GeoLocations get success';
export const SAVE_GEOLOCATION = "Save new geoLocation";
export const DELETE_GEOLOCATION = 'Delete geoLocation';
export const UPDATE_GEOLOCATION = 'Update geoLocation';

export class GetMetadata implements Action {
    readonly type = GET_GEOLOCATION_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_GEOLOCATION_META_SUCCESS;
    constructor(public payload: _GeoLocationMeta) { }
}

export class GetEntries implements Action {
    readonly type = GET_GEOLOCATIONS;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_GEOLOCATIONS_SUCCESS;
    constructor(public payload: { entries: { [id: number]: _GeoLocation }, ids: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_GEOLOCATION;
    constructor(public payload: _GeoLocation) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_GEOLOCATION;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_GEOLOCATION;
    constructor(public payload: { entry: _GeoLocation, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;