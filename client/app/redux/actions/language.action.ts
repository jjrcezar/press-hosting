import { Action } from '@ngrx/store';
import { _Language, _LanguageMeta } from '../models/language.model';

export const GET_LANGUAGE = 'Language get';
export const GET_LANGUAGE_META = 'Language metadata get';
export const GET_LANGUAGE_META_SUCCESS = 'Language metadata get success';
export const GET_LANGUAGES = 'Languages get';
export const GET_LANGUAGES_SUCCESS = 'Languages get success';
export const SAVE_LANGUAGE = "Save new language";
export const DELETE_LANGUAGE = 'Delete language';
export const UPDATE_LANGUAGE = 'Update language';

export class GetMetadata implements Action {
    readonly type = GET_LANGUAGE_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_LANGUAGE_META_SUCCESS;
    constructor(public payload: _LanguageMeta) { }
}

export class GetEntries implements Action {
    readonly type = GET_LANGUAGES;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_LANGUAGES_SUCCESS;
    constructor(public payload: { languages: { [id: number]: _Language }, list: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_LANGUAGE;
    constructor(public payload: _Language) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_LANGUAGE;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_LANGUAGE;
    constructor(public payload: { entry: _Language, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;