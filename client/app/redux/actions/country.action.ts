import { Action } from '@ngrx/store';
import { _Country } from '../models/country.model';

export const GET_COUNTRY = 'Country get';
export const GET_COUNTRY_META = 'Country meta get';
export const GET_COUNTRY_META_SUCCESS = 'Country meta get success';
export const GET_COUNTRIES = 'Countries get';
export const GET_COUNTRIES_SUCCESS = 'Countries get success';
export const SAVE_COUNTRY = "Save new country";
export const DELETE_COUNTRY = 'Delete country';
export const UPDATE_COUNTRY = 'Update country';

export class GetMetadata implements Action {
    readonly type = GET_COUNTRY_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_COUNTRY_META_SUCCESS;
    constructor(public payload: _Country) { }
}

export class GetEntries implements Action {
    readonly type = GET_COUNTRIES;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_COUNTRIES_SUCCESS;
    constructor(public payload: { entries: { [id: number]: _Country }, ids: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_COUNTRY;
    constructor(public payload: _Country) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_COUNTRY;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_COUNTRY;
    constructor(public payload: { entry: _Country, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;