import { Action } from '@ngrx/store';
import { _State } from '../models/state.model';

export const GET_STATE = 'State get';
export const GET_STATE_META = 'State meta get';
export const GET_STATE_META_SUCCESS = 'State meta get success';
export const GET_STATES = 'States get';
export const GET_STATES_SUCCESS = 'States get success';
export const SAVE_STATE = "Save new state";
export const DELETE_STATE = 'Delete state';
export const UPDATE_STATE = 'Update state';

export class GetMetadata implements Action {
    readonly type = GET_STATE_META;
}

export class GetMetadataSuccess implements Action {
    readonly type = GET_STATE_META_SUCCESS;
    constructor(public payload: _State) { }
}

export class GetEntries implements Action {
    readonly type = GET_STATES;
}

export class GetEntriesSuccess implements Action {
    readonly type = GET_STATES_SUCCESS;
    constructor(public payload: { entries: { [id: number]: _State }, ids: number[] }) { }
}

export class SaveEntry implements Action {
    readonly type = SAVE_STATE;
    constructor(public payload: _State) { }
}

export class DeleteEntry implements Action {
    readonly type = DELETE_STATE;
    constructor(public payload: number) { }
}

export class UpdateEntry implements Action {
    readonly type = UPDATE_STATE;
    constructor(public payload: { entry: _State, id: number }) { }
}

export type All = GetMetadata | GetMetadataSuccess | GetEntries | GetEntriesSuccess | SaveEntry | DeleteEntry | UpdateEntry;