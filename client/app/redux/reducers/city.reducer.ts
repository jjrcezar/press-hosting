
import * as CityActions from '../actions/city.action';
import { CityAppState, cityInitialState } from '../models/city.model';

export type Action = CityActions.All;

export function cityReducer(state: CityAppState = cityInitialState, action: Action) {
    switch (action.type) {
        case CityActions.GET_CITY_META: {
            return { ...state, loading: true };
        }
        case CityActions.GET_CITY_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case CityActions.GET_CITIES: {
            return { ...state, loading: true };
        }
        case CityActions.GET_CITIES_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case CityActions.SAVE_CITY: {
            return { ...state, loading: true };
        }
        case CityActions.DELETE_CITY: {
            return { ...state, loading: true };
        }
        case CityActions.UPDATE_CITY: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}