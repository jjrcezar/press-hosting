
import * as GeoLocationActions from '../actions/geoLocation.action';
import { GeoLocationAppState, geoLocationInitialState } from '../models/geoLocation.model';

export type Action = GeoLocationActions.All;

export function geoLocationReducer(state: GeoLocationAppState = geoLocationInitialState, action: Action) {
    switch (action.type) {
        case GeoLocationActions.GET_GEOLOCATION_META: {
            return { ...state, loading: true };
        }
        case GeoLocationActions.GET_GEOLOCATION_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case GeoLocationActions.GET_GEOLOCATIONS: {
            return { ...state, loading: true };
        }
        case GeoLocationActions.GET_GEOLOCATIONS_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case GeoLocationActions.SAVE_GEOLOCATION: {
            return { ...state, loading: true };
        }
        case GeoLocationActions.DELETE_GEOLOCATION: {
            return { ...state, loading: true };
        }
        case GeoLocationActions.UPDATE_GEOLOCATION: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}