
import * as CurrencyActions from '../actions/currency.action';
import { CurrencyAppState, currencyInitialState } from '../models/currency.model';

export type Action = CurrencyActions.All;

export function currencyReducer(state: CurrencyAppState = currencyInitialState, action: Action) {
    switch (action.type) {
        case CurrencyActions.GET_CURRENCY_META: {
            return { ...state, loading: true };
        }
        case CurrencyActions.GET_CURRENCY_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case CurrencyActions.GET_CURRENCIES: {
            return { ...state, loading: true };
        }
        case CurrencyActions.GET_CURRENCIES_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case CurrencyActions.SAVE_CURRENCY: {
            return { ...state, loading: true };
        }
        case CurrencyActions.DELETE_CURRENCY: {
            return { ...state, loading: true };
        }
        case CurrencyActions.UPDATE_CURRENCY: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}