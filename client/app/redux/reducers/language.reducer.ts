
import * as LanguageActions from '../actions/language.action';
import { LanguageAppState, languageInitialState } from '../models/language.model';

export type Action = LanguageActions.All;

export function languageReducer(state: LanguageAppState = languageInitialState, action: Action) {
    switch (action.type) {
        case LanguageActions.GET_LANGUAGE_META: {
            return { ...state, loading: true };
        }
        case LanguageActions.GET_LANGUAGE_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case LanguageActions.GET_LANGUAGES: {
            return { ...state, loading: true };
        }
        case LanguageActions.GET_LANGUAGES_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case LanguageActions.SAVE_LANGUAGE: {
            return { ...state, loading: true };
        }
        case LanguageActions.DELETE_LANGUAGE: {
            return { ...state, loading: true };
        }
        case LanguageActions.UPDATE_LANGUAGE: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}