
import * as StateActions from '../actions/state.action';
import { StateAppState, stateInitialState } from '../models/state.model';

export type Action = StateActions.All;

export function stateReducer(state: StateAppState = stateInitialState, action: Action) {
    switch (action.type) {
        case StateActions.GET_STATE_META: {
            return { ...state, loading: true };
        }
        case StateActions.GET_STATE_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case StateActions.GET_STATES: {
            return { ...state, loading: true };
        }
        case StateActions.GET_STATES_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case StateActions.SAVE_STATE: {
            return { ...state, loading: true };
        }
        case StateActions.DELETE_STATE: {
            return { ...state, loading: true };
        }
        case StateActions.UPDATE_STATE: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}