
import * as CountryActions from '../actions/country.action';
import { CountryAppState, countryInitialState } from '../models/country.model';

export type Action = CountryActions.All;

export function countryReducer(state: CountryAppState = countryInitialState, action: Action) {
    switch (action.type) {
        case CountryActions.GET_COUNTRY_META: {
            return { ...state, loading: true };
        }
        case CountryActions.GET_COUNTRY_META_SUCCESS: {
            let metadata = action.payload;
            return { ...state, metadata, loading: false };
        }
        case CountryActions.GET_COUNTRIES: {
            return { ...state, loading: true };
        }
        case CountryActions.GET_COUNTRIES_SUCCESS: {
            return { ...state, ...action.payload, loading: false };
        }
        case CountryActions.SAVE_COUNTRY: {
            return { ...state, loading: true };
        }
        case CountryActions.DELETE_COUNTRY: {
            return { ...state, loading: true };
        }
        case CountryActions.UPDATE_COUNTRY: {
            return { ...state, loading: true };
        }
        default: {
            return state;
        }
    }
}