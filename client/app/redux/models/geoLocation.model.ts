import { EntityMemberMeta } from '../app.state';

export type _GeoLocation = {
    id: number,
    latitude: number,
    longitude: number,
    status: string
};

export interface _GeoLocationMeta {
    id?: EntityMemberMeta,
    latitude?: EntityMemberMeta,
    longitude?: EntityMemberMeta,
    status?: EntityMemberMeta
}

export interface GeoLocationAppState {
    entries: { [id: number]: _GeoLocation },
    ids: number[],
    metadata: _GeoLocationMeta,
    loading: boolean
}

export const geoLocationInitialState: GeoLocationAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};