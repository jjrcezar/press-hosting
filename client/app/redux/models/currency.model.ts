import { EntityMemberMeta } from '../app.state';

export type _Currency = {
    id: number,
    name: string,
    symbol: string,
    isoAlpha3: string,
    isoNumeric: number,
    languageId: number,
    status: string
};

export interface _CurrencyMeta {
    id?: EntityMemberMeta,
    name?: EntityMemberMeta,
    symbol?: EntityMemberMeta,
    isoAlpha3?: EntityMemberMeta,
    isoNumeric?: EntityMemberMeta,
    languageId?: EntityMemberMeta,
    status?: EntityMemberMeta
}

export interface CurrencyAppState {
    entries: { [id: number]: _Currency },
    ids: number[],
    metadata: _CurrencyMeta,
    loading: boolean
}

export const currencyInitialState: CurrencyAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};