import { EntityMemberMeta } from '../app.state';

export type _Country = {
    id: number,
    name: string,
    isoAlpha2: string,
    isoAlpha3: string,
    isoNumeric: string,
    languageId: number,
    currencyId: number,
    geoLocationId: number,
    status: string
};

export interface _CountryMeta {
    id?: EntityMemberMeta,
    name?: EntityMemberMeta,
    isoAlpha2?: EntityMemberMeta,
    isoAlpha3?: EntityMemberMeta,
    isoNumeric?: EntityMemberMeta,
    languageId?: EntityMemberMeta,
    currencyId?: EntityMemberMeta,
    geoLocationId?: EntityMemberMeta,
    status?: EntityMemberMeta
}

export interface CountryAppState {
    entries: { [id: number]: _Country },
    ids: number[],
    metadata: _CountryMeta,
    loading: boolean
}

export const countryInitialState: CountryAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};