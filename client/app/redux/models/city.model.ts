import { EntityMemberMeta } from '../app.state';

export interface _City {
    id: number,
    name: string,
    languageId: number,
    stateId: number,
    geoLocationId: number,
    status: string
};

export interface _CityMeta {
    id?: EntityMemberMeta,
    name?: EntityMemberMeta,
    languageId?: EntityMemberMeta,
    stateId?: EntityMemberMeta,
    geoLocationId?: EntityMemberMeta,
    status?: EntityMemberMeta
}

export interface CityAppState {
    entries: { [id: number]: _City },
    ids: number[],
    metadata: _CityMeta,
    loading: boolean
}

export const cityInitialState: CityAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};