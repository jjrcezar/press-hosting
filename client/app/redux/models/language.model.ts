import { EntityMemberMeta } from '../app.state';

//language entity's field values
export interface _Language {
    id: number,
    name: string,
    isoAlpha2: string,
    isoAlpha3: string,
    status: string
};

//language entity's metadata
export interface _LanguageMeta {
    id?: EntityMemberMeta,
    name?: EntityMemberMeta,
    isoAlpha2?: EntityMemberMeta,
    isoAlpha3?: EntityMemberMeta,
    status?: EntityMemberMeta
}

//language entity's app state
export interface LanguageAppState {
    entries: { [id: number]: _Language },
    ids: number[],
    metadata: _LanguageMeta,
    loading: boolean
}

export const languageInitialState: LanguageAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};