import { EntityMemberMeta } from '../app.state';

export type _State = {
    id: number,
    name: string,
    languageId: number,
    countryId: number,
    geoLocationId: number,
    status: string
};

export interface _StateMeta {
    id?: EntityMemberMeta,
    name?: EntityMemberMeta,
    languageId?: EntityMemberMeta,
    countryId?: EntityMemberMeta,
    geoLocationId?: EntityMemberMeta,
    status?: EntityMemberMeta
}

export interface StateAppState {
    entries: { [id: number]: _State },
    ids: number[],
    metadata: _StateMeta,
    loading: boolean
}

export const stateInitialState: StateAppState = {
    entries: [],
    ids: [],
    metadata: {},
    loading: false
};