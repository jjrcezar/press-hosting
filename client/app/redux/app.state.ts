import { CityAppState, cityInitialState } from './models/city.model';
import { CountryAppState, countryInitialState } from './models/country.model';
import { CurrencyAppState, currencyInitialState } from './models/currency.model';
import { GeoLocationAppState, geoLocationInitialState } from './models/geoLocation.model';
import { LanguageAppState, languageInitialState } from './models/language.model';
import { StateAppState, stateInitialState } from './models/state.model';

//entity member's metadata
export interface EntityMemberMeta {
    dataType: string,
    inputType?: string,
    readOnly: boolean,
    label: string,
    description?: string,
    hint?: string,
    validation: {
        type: string,
        required: boolean,
        values?: string[],
        maxLength?: number,
        format?: string
    }
}

//application state
export interface AppState {
    cities: CityAppState,    
    countries: CountryAppState,    
    currencies: CurrencyAppState,    
    geoLocations: GeoLocationAppState,
    languages: LanguageAppState,
    states: StateAppState
}

//application state's initial value
export const initialState: AppState = {
    cities: cityInitialState,
    countries: countryInitialState,    
    currencies: currencyInitialState,
    geoLocations: geoLocationInitialState,
    languages: languageInitialState,
    states: stateInitialState
};