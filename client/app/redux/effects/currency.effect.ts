import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import * as CurrencyActions from '../actions/currency.action';

import { MetainfoService } from '../../services/metainfo.service';
import { CurrencyService } from '../../services/currency.service';

export type Action = CurrencyActions.All;

@Injectable()
export class CurrencyEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _currencyService: CurrencyService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(CurrencyActions.GET_CURRENCY_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.CURRENCY))
        .map(meta => new CurrencyActions.GetMetadataSuccess(meta.currency.members));

    @Effect() getCurrencies: Observable<Action> = this._actions.ofType(CurrencyActions.GET_CURRENCIES)
        .switchMap(payload => this._currencyService.findAll())
        .map(currencies => new CurrencyActions.GetEntriesSuccess(currencies));

    @Effect() saveCurrency: Observable<Action> = this._actions.ofType(CurrencyActions.SAVE_CURRENCY)
        .mergeMap((payload: any) => this._currencyService.saveEntry(payload.payload))
        .map(res => new CurrencyActions.GetEntries());

    @Effect() deleteCurrency: Observable<Action> = this._actions.ofType(CurrencyActions.DELETE_CURRENCY)
        .mergeMap((payload: any) => this._currencyService.deleteEntry(payload.payload))
        .map(res => new CurrencyActions.GetEntries());

    @Effect() updateCurrency: Observable<Action> = this._actions.ofType(CurrencyActions.UPDATE_CURRENCY)
        .mergeMap((payload: any) => this._currencyService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new CurrencyActions.GetEntries());
}