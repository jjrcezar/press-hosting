import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import * as GeoLocationActions from '../actions/geoLocation.action';

import { MetainfoService } from '../../services/metainfo.service';
import { GeoLocationService } from '../../services/geoLocation.service';

export type Action = GeoLocationActions.All;

@Injectable()
export class GeoLocationEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _geoLocationService: GeoLocationService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(GeoLocationActions.GET_GEOLOCATION_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.GEOLOCATION))
        .map(meta => new GeoLocationActions.GetMetadataSuccess(meta.geoLocation.members));

    @Effect() getGeoLocations: Observable<Action> = this._actions.ofType(GeoLocationActions.GET_GEOLOCATIONS)
        .switchMap(payload => this._geoLocationService.findAll())
        .map(geoLocations => new GeoLocationActions.GetEntriesSuccess(geoLocations));

    @Effect() saveGeoLocation: Observable<Action> = this._actions.ofType(GeoLocationActions.SAVE_GEOLOCATION)
        .mergeMap((payload: any) => this._geoLocationService.saveEntry(payload.payload))
        .map(res => new GeoLocationActions.GetEntries());

    @Effect() deleteGeoLocation: Observable<Action> = this._actions.ofType(GeoLocationActions.DELETE_GEOLOCATION)
        .mergeMap((payload: any) => this._geoLocationService.deleteEntry(payload.payload))
        .map(res => new GeoLocationActions.GetEntries());

    @Effect() updateGeoLocation: Observable<Action> = this._actions.ofType(GeoLocationActions.UPDATE_GEOLOCATION)
        .mergeMap((payload: any) => this._geoLocationService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new GeoLocationActions.GetEntries());
}