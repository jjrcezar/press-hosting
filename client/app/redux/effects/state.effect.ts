import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import * as StateActions from '../actions/state.action';

import { MetainfoService } from '../../services/metainfo.service';
import { StateService } from '../../services/state.service';

export type Action = StateActions.All;

@Injectable()
export class StateEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _stateService: StateService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(StateActions.GET_STATE_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.STATE))
        .map(meta => new StateActions.GetMetadataSuccess(meta.state.members));

    @Effect() getStates: Observable<Action> = this._actions.ofType(StateActions.GET_STATES)
        .switchMap(payload => this._stateService.findAll())
        .map(states => new StateActions.GetEntriesSuccess(states));

    @Effect() saveState: Observable<Action> = this._actions.ofType(StateActions.SAVE_STATE)
        .mergeMap((payload: any) => this._stateService.saveEntry(payload.payload))
        .map(res => new StateActions.GetEntries());

    @Effect() deleteState: Observable<Action> = this._actions.ofType(StateActions.DELETE_STATE)
        .mergeMap((payload: any) => this._stateService.deleteEntry(payload.payload))
        .map(res => new StateActions.GetEntries());

    @Effect() updateState: Observable<Action> = this._actions.ofType(StateActions.UPDATE_STATE)
        .mergeMap((payload: any) => this._stateService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new StateActions.GetEntries());
}