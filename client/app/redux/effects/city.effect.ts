import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import 'rxjs/add/operator/catch';

import * as CityActions from '../actions/city.action';

import { MetainfoService } from '../../services/metainfo.service';
import { CityService } from '../../services/city.service';

export type Action = CityActions.All;

@Injectable()
export class CityEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _cityService: CityService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(CityActions.GET_CITY_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.CITY))
        .map(meta => new CityActions.GetMetadataSuccess(meta.city.members));

    @Effect() getCities: Observable<Action> = this._actions.ofType(CityActions.GET_CITIES)
        .switchMap(payload => this._cityService.findAll())
        .map(cities => new CityActions.GetEntriesSuccess(cities));

    @Effect() saveCity: Observable<Action> = this._actions.ofType(CityActions.SAVE_CITY)
        .mergeMap((payload: any) => this._cityService.saveEntry(payload.payload))
        .map(res => new CityActions.GetEntries());

    @Effect() deleteCity: Observable<Action> = this._actions.ofType(CityActions.DELETE_CITY)
        .mergeMap((payload: any) => this._cityService.deleteEntry(payload.payload))
        .map(res => new CityActions.GetEntries());

    @Effect() updateCity: Observable<Action> = this._actions.ofType(CityActions.UPDATE_CITY)
        .mergeMap((payload: any) => this._cityService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new CityActions.GetEntries());
}