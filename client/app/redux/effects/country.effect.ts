import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import * as CountryActions from '../actions/country.action';

import { MetainfoService } from '../../services/metainfo.service';
import { CountryService } from '../../services/country.service';

export type Action = CountryActions.All;

@Injectable()
export class CountryEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _countryService: CountryService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(CountryActions.GET_COUNTRY_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.COUNTRY))
        .map(meta => new CountryActions.GetMetadataSuccess(meta.country.members));

    @Effect() getCountries: Observable<Action> = this._actions.ofType(CountryActions.GET_COUNTRIES)
        .switchMap(payload => this._countryService.findAll())
        .map(countries => new CountryActions.GetEntriesSuccess(countries));

    @Effect() saveCountry: Observable<Action> = this._actions.ofType(CountryActions.SAVE_COUNTRY)
        .mergeMap((payload: any) => this._countryService.saveEntry(payload.payload))
        .map(res => new CountryActions.GetEntries());

    @Effect() deleteCountry: Observable<Action> = this._actions.ofType(CountryActions.DELETE_COUNTRY)
        .mergeMap((payload: any) => this._countryService.deleteEntry(payload.payload))
        .map(res => new CountryActions.GetEntries());

    @Effect() updateCountry: Observable<Action> = this._actions.ofType(CountryActions.UPDATE_COUNTRY)
        .mergeMap((payload: any) => this._countryService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new CountryActions.GetEntries());
}