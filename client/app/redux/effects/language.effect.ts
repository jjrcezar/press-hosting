import { Injectable } from "@angular/core";
import { Actions, Effect } from '@ngrx/effects';

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import * as LanguageActions from '../actions/language.action';

import { MetainfoService } from '../../services/metainfo.service';
import { LanguageService } from '../../services/language.service';

export type Action = LanguageActions.All;

@Injectable()
export class LanguageEffects {

    constructor(private _actions: Actions,
        private _metainfoService: MetainfoService,
        private _languageService: LanguageService) { }

    @Effect() getMetainfo: Observable<Action> = this._actions.ofType(LanguageActions.GET_LANGUAGE_META)
        .switchMap(payload => this._metainfoService.get(this._metainfoService.ENTITY.LANGUAGE))
        .map(meta => new LanguageActions.GetMetadataSuccess(meta.language.members));

    @Effect() getLanguages: Observable<Action> = this._actions.ofType(LanguageActions.GET_LANGUAGES)
        .switchMap(payload => this._languageService.findAll())
        .map(languages => new LanguageActions.GetEntriesSuccess(languages));

    @Effect() saveLanguage: Observable<Action> = this._actions.ofType(LanguageActions.SAVE_LANGUAGE)
        .mergeMap((payload: any) => this._languageService.saveEntry(payload.payload))
        .map(res => new LanguageActions.GetEntries());

    @Effect() deleteLanguage: Observable<Action> = this._actions.ofType(LanguageActions.DELETE_LANGUAGE)
        .mergeMap((payload: any) => this._languageService.deleteEntry(payload.payload))
        .map(res => new LanguageActions.GetEntries());

    @Effect() updateLanguage: Observable<Action> = this._actions.ofType(LanguageActions.UPDATE_LANGUAGE)
        .mergeMap((payload: any) => this._languageService.updateEntry(payload.payload.id, payload.payload.entry))
        .map(res => new LanguageActions.GetEntries());
}