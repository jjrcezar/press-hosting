import { Injectable, Inject } from '@angular/core';
import { TRANSLATIONS } from '../shared/i18n/translate.provider';

@Injectable()
export class TranslateService {
    private _currentLang: string = "en-US";

    public get currentLang() {
        return this._currentLang;
    }

    constructor( @Inject(TRANSLATIONS) private _translations: any) {
        this._currentLang = `${navigator.language}`;
        // this._currentLang = "de-DE";
    }

    public use(lang: string): void {
        this._currentLang = lang;
    }

    private translate(key: string): string {
        let translation = key;
        if (this._translations[this.currentLang] &&
            this._translations[this.currentLang][key]) {
            return this._translations[this.currentLang][key];
        }
        return translation;
    }

    public instant(key: string) {
        return this.translate(key);
    }

    public getCurrentLocale() {
        return this._currentLang;
    }
}