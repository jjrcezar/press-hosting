import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpInterceptorService, InterceptableHttp, getHttpHeadersOrInit } from 'ng-http-interceptor';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';
import { Constants } from '../shared/constants';

@Injectable()
export class CurrencyService {
  constructor(
    private _auth: AuthService,
    private _http: Http,
    private _interceptableHttp: InterceptableHttp,
    private _httpInterceptor: HttpInterceptorService) {
    _httpInterceptor.request().addInterceptor(_auth.tokenInterceptor);
  }

  findAll(): Observable<any> {
    return this._interceptableHttp.get(`${environment.API_URL}${Constants.CURRENCIES_URL}`)
      .map(res => res.json())
      .map(res => {
        const data = res.data.items;
        const entries = data.reduce((acc, l) => (acc[l.id] = l, acc), {});
        const ids = data.map(l => l.id);
        return { entries, ids };
      }
      );
  }

  saveEntry(entry): Observable<any> {
    return this._interceptableHttp.post(`${environment.API_URL}${Constants.CURRENCIES_URL}`, entry)
      .map(res => res.json());
  }

  deleteEntry(id): Observable<any> {
    return this._interceptableHttp.delete(`${environment.API_URL}${Constants.CURRENCIES_URL}/${id}`)
      .map(res => res.json());
  }

  updateEntry(id, entry): Observable<any> {
    return this._interceptableHttp.put(`${environment.API_URL}${Constants.CURRENCIES_URL}/${id}`, entry)
      .map(res => res.json());
  }
}
