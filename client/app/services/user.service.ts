import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Constants } from '../shared/constants';

@Injectable()
export class UserService {

  private _headers = new Headers({ 'Content-Type': 'application/json' });
  private _options = new RequestOptions({ headers: this._headers });

  constructor(private _http: Http) { }

  adminLogin(credentials): Observable<any> {
    return this._http.post(`${environment.API_URL}${Constants.ADMIN_LOGIN_URL}`, JSON.stringify(credentials), this._options);
  }
}
