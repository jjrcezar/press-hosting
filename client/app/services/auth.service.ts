import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { getHttpHeadersOrInit } from 'ng-http-interceptor';

import { UserService } from '../services/user.service';

@Injectable()
export class AuthService {
  loggedIn = false;
  currentUser = { username: '', authToken: '' };

  constructor(private _userService: UserService,
    private _router: Router) {
    const username = localStorage.getItem('username');
    const authToken = localStorage.getItem('authToken');
    if (username && authToken) {
      this.setCurrentUser({ username: username, authToken: authToken });
    }
  }

  adminLogin(usernameAndPassword) {
    return this._userService.adminLogin(usernameAndPassword).map(res => res.json()).map(
      res => {
        if (res.Authorization) {
          this.setCurrentUser({ username: usernameAndPassword.username, authToken: res.Authorization });
          localStorage.setItem('username', this.currentUser.username);
          localStorage.setItem('authToken', this.currentUser.authToken);
          return this.loggedIn;
        } else {
          return res;
        }
      }
    );
  }

  logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('authToken');
    this.loggedIn = false;
    this.currentUser = { username: '', authToken: '' };
    this._router.navigate(['/login']);
  }

  // decodeUserFromToken(token) {
  //   return this.jwtHelper.decodeToken(token).user;
  // }

  setCurrentUser(decodedUser) {
    this.loggedIn = true;
    this.currentUser.username = decodedUser.username;
    this.currentUser.authToken = decodedUser.authToken;
  }

  tokenInterceptor = (data, method) => {
    const headers = getHttpHeadersOrInit(data, method);
    headers.set('Authorization', `Bearer ${this.currentUser.authToken}`);
    return data;
  };

}
