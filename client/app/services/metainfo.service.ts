import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpInterceptorService, InterceptableHttp, getHttpHeadersOrInit } from 'ng-http-interceptor';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';
import { Constants } from '../shared/constants';

@Injectable()
export class MetainfoService {

  readonly ENTITY = {
    USER: Constants.METAINFO_USER_URL,
    CITY: Constants.METAINFO_CITY_URL,
    COUNTRY: Constants.METAINFO_COUNTRY_URL,
    CURRENCY: Constants.METAINFO_CURRENCY_URL,
    GEOLOCATION: Constants.METAINFO_GEOLOCATION_URL,
    LANGUAGE: Constants.METAINFO_LANGUAGE_URL,
    STATE: Constants.METAINFO_STATE_URL,
  }

  constructor(
    private _auth: AuthService,
    private _http: Http,
    private _interceptableHttp: InterceptableHttp,
    private _httpInterceptor: HttpInterceptorService) {
    _httpInterceptor.request().addInterceptor(_auth.tokenInterceptor);
  }

  get(entity): Observable<any> {
    return this._interceptableHttp.get(`${environment.API_URL}${entity}`).map(res => res.json()).map(
      data => {
        return data && data.metadata || {};
      }
    );
  }
}
